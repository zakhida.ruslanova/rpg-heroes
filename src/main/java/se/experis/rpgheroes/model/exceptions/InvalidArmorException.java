package se.experis.rpgheroes.model.exceptions;

public class InvalidArmorException extends Exception {
    public InvalidArmorException(String errorMessage) {
        super(errorMessage);
    }
}