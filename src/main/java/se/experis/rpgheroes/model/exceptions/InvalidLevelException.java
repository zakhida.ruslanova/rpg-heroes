package se.experis.rpgheroes.model.exceptions;

public class InvalidLevelException extends Exception {
    public InvalidLevelException(String errorMessage) {
        super(errorMessage);
    }
}