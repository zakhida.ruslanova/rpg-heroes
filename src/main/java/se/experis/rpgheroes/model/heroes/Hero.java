package se.experis.rpgheroes.model.heroes;

import se.experis.rpgheroes.model.exceptions.InvalidArmorException;
import se.experis.rpgheroes.model.exceptions.InvalidLevelException;
import se.experis.rpgheroes.model.exceptions.InvalidWeaponException;
import se.experis.rpgheroes.model.items.*;
import se.experis.rpgheroes.model.items.enums.ArmorType;
import se.experis.rpgheroes.model.items.enums.Slot;
import se.experis.rpgheroes.model.items.enums.WeaponType;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class Hero {
    private final String name;
    private String heroClass;
    public int level;
    private final HeroAttribute heroAttribute;
    private final HeroAttribute levelAttribute;
    private final HashMap<Slot, Item> equipment;
    private final ArrayList<WeaponType> validWeaponTypes;
    private final ArrayList<ArmorType> validArmorTypes;

    public Hero(String name, String heroClass) {
        this.name = name;
        this.heroClass = heroClass;
        this.level = 1;
        this.heroAttribute = getHeroAttributes(heroClass);
        this.levelAttribute = getHeroAttributes(heroClass);
        this.equipment = getDefaultEquipment();

        validWeaponTypes = new ArrayList<>();
        validArmorTypes = new ArrayList<>();

        switch (heroClass) {
            case "Mage" -> {
                validWeaponTypes.add(WeaponType.WANDS);
                validWeaponTypes.add(WeaponType.STAFFS);
                validArmorTypes.add(ArmorType.CLOTH);
            }
            case "Ranger" -> {
                validWeaponTypes.add(WeaponType.BOWS);
                validArmorTypes.add(ArmorType.LEATHER);
                validArmorTypes.add(ArmorType.MAIL);
            }
            case "Rogue" -> {
                validWeaponTypes.add(WeaponType.DAGGERS);
                validWeaponTypes.add(WeaponType.SWORDS);
                validArmorTypes.add(ArmorType.LEATHER);
                validArmorTypes.add(ArmorType.MAIL);
            }
            case "Warrior" -> {
                validWeaponTypes.add(WeaponType.AXES);
                validWeaponTypes.add(WeaponType.HAMMERS);
                validWeaponTypes.add(WeaponType.SWORDS);
                validArmorTypes.add(ArmorType.MAIL);
                validArmorTypes.add(ArmorType.PLATE);
            }
            default -> throw new IllegalArgumentException("The name " + heroClass + " is not match heroes name");
        }
    }

    public HashMap<Slot, Item> getDefaultEquipment() {
        HashMap<Slot, Item> createNewEquipment = new HashMap<>();
        createNewEquipment.put(Slot.BODY, null);
        createNewEquipment.put(Slot.HEAD, null);
        createNewEquipment.put(Slot.LEGS, null);
        createNewEquipment.put(Slot.WEAPON, null);
        return createNewEquipment;
    }

    public HeroAttribute getHeroAttributes(String heroClass) {
        return switch (heroClass) {
            case "Mage" -> new HeroAttribute(1, 1, 8);
            case "Ranger" -> new HeroAttribute(1, 7, 1);
            case "Rogue" -> new HeroAttribute(2, 6, 1);
            case "Warrior" -> new HeroAttribute(5, 2, 1);
            default -> throw new IllegalArgumentException("The name " + heroClass + " is not match heroes name");
        };
    }

    public HeroAttribute levelUp() {

        this.level++;

        return switch (getHeroClass()) {
            case "Mage" -> levelAttribute.heroAttributeIncrease(1, 1, 5);
            case "Ranger" -> levelAttribute.heroAttributeIncrease(1, 5, 1);
            case "Rogue" -> levelAttribute.heroAttributeIncrease(1, 4, 1);
            case "Warrior" -> levelAttribute.heroAttributeIncrease(3, 2, 1);
            default -> throw new IllegalArgumentException("The name " + heroClass + " is not match heroes name");
        };
    }

    public boolean equipWeapons(Weapon weapon) throws InvalidWeaponException, InvalidLevelException {
        boolean levelToLow = level < weapon.getRequiredLevel();
        boolean invalidWeaponType = validWeaponTypes.stream().noneMatch(weaponType -> weaponType == weapon.getWeaponType());

        if (invalidWeaponType) {
            throw new InvalidWeaponException(heroClass + " cannot equip weapon type " + weapon.getWeaponType());
        } else if (levelToLow) {
            throw new InvalidLevelException(level + " is not enougth. The required level is: " + weapon.getRequiredLevel());
        }
        equipment.put(weapon.getSlot(), weapon);
        return true;
    }

    //TODO: NOTE: When equipping a new item, don’t worry about the old item, just replace it. It does not need to be stored anywhere.

    public boolean equipArmor(Armor armor) throws InvalidArmorException, InvalidLevelException {
        boolean levelToLow = level < armor.getRequiredLevel();
        boolean invalidArmorType = validArmorTypes.stream().noneMatch(armorType -> armorType == armor.getArmorType());

        if (invalidArmorType) {
            throw new InvalidArmorException(heroClass + " cannot equip armor type " + armor.getArmorType());
        } else if (levelToLow) {
            throw new InvalidLevelException(level + " is not enougth. The required level is: " + armor.getRequiredLevel());
        }
        equipment.put(armor.getSlot(), armor);
        return true;
    }

    public double damage() {
        return switch (getHeroClass()) {
            case "Mage" -> getDamagingAttribute(getLevelAttribute().getIntelligence());
            case "Ranger", "Rogue" -> getDamagingAttribute(getLevelAttribute().getDexterity());
            case "Warrior" -> getDamagingAttribute(getLevelAttribute().getStrength());
            default -> throw new IllegalArgumentException("The name " + heroClass + " is not match heroes name");
        };
    }

    private double getDamagingAttribute(int damageAttribute) {
        double damage;
        if (equipment.get(Slot.WEAPON) == null) {
            damage = 1 + ((double) damageAttribute / 100);
        } else {
            Weapon weapon = (Weapon) getEquipment().get(Slot.WEAPON);
            damage = weapon.getWeaponDamage() * (1 + ((double) damageAttribute / 100));
        }
        return damage;
    }

    public String totalAttribute() {
        int calculatedArmorAttributes = getEquipment().values()
                .stream()
                .filter(Armor.class::isInstance)
                .map(Armor.class::cast)
                .mapToInt(armor -> (armor.getArmorAttribute().getStrength() +
                        armor.getArmorAttribute().getDexterity() +
                        armor.getArmorAttribute().getIntelligence()))
                .reduce(0, Integer::sum);

        int calculateLevelAttribute =
                getLevelAttribute().getStrength() +
                        getLevelAttribute().getDexterity() +
                        getLevelAttribute().getIntelligence();

        var total = calculateLevelAttribute + calculatedArmorAttributes;

        return "Total Attribute: " + total;
    }

    public String display() {
        StringBuilder sb = new StringBuilder();
        sb.append("Name: " + getName());
        sb.append("\n" + "Class: " + getHeroClass());
        sb.append("\n" + "Level: " + getLevel());
        sb.append("\n" + "Total strength: " + getLevelAttribute().getStrength());
        sb.append("\n" + "Total dexterity: " + getLevelAttribute().getDexterity());
        sb.append("\n" + "Total intelligence: " + getLevelAttribute().getIntelligence());
        sb.append("\n" + "Damage: " + damage());
       return sb.toString();
    }

    public String getName() {
        return name;
    }

    public HeroAttribute getHeroAttribute() {
        return heroAttribute;
    }

    public int getLevel() {
        return this.level;
    }

    public String getHeroClass() {
        return heroClass;
    }

    public void setHeroClass(String heroClass) {
        this.heroClass = heroClass;
    }

    public HeroAttribute getLevelAttribute() {
        return levelAttribute;
    }

    public HashMap<Slot, Item> getEquipment() {
        return equipment;
    }

    @Override
    public String toString() {
        return "Name:'" + getName() + '\''
                + ", Hero:'" + getHeroClass() + '\''
                + ", Level:" + getLevel()
                + ", Strength: " + getHeroAttribute().getStrength()
                + ", Dexterity: " + getHeroAttribute().getDexterity()
                + ", Intelligence: " + getHeroAttribute().getIntelligence();
    }
}