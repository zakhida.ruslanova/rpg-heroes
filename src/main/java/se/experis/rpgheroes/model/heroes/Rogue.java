package se.experis.rpgheroes.model.heroes;

import se.experis.rpgheroes.model.exceptions.InvalidArmorException;
import se.experis.rpgheroes.model.exceptions.InvalidLevelException;
import se.experis.rpgheroes.model.exceptions.InvalidWeaponException;
import se.experis.rpgheroes.model.items.Armor;
import se.experis.rpgheroes.model.items.Weapon;

public class Rogue extends Hero{
    public Rogue(String name) {
        super(name, "Rogue");
    }

    @Override
    public boolean equipWeapons(Weapon weapon) throws InvalidWeaponException, InvalidLevelException {
        return super.equipWeapons(weapon);
    }
    @Override
    public boolean equipArmor(Armor armor) throws InvalidArmorException, InvalidLevelException {
        return super.equipArmor(armor);
    }
    @Override
    public double damage() {
        return super.damage();
    }
    @Override
    public String totalAttribute() {
        return  super.totalAttribute();
    }
    @Override
    public String display() {
        return super.display();
    }
}