package se.experis.rpgheroes.model.items;

import se.experis.rpgheroes.model.items.enums.Slot;

public abstract class Item {
    private String name;
    private final int requiredLevel;
    private final Slot slot;

    public Item(String name, int requiredLevel, Slot slot) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public Slot getSlot() {
        return slot;
    }
}