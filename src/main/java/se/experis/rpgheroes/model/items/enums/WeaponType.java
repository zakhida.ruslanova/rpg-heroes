package se.experis.rpgheroes.model.items.enums;

public enum WeaponType {
    AXES, BOWS, DAGGERS, HAMMERS, STAFFS, SWORDS, WANDS
}