package se.experis.rpgheroes.model.items;

public class HeroAttribute {
    int strength;
    int dexterity;
    int intelligence;

    public HeroAttribute(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    public HeroAttribute heroAttributeIncrease(int strengthNumber, int dexterityNumber, int intelligenceNumber) {
        var strengthUpdated = this.strength = getStrength() + strengthNumber;
        var dexterityUpdated = this.dexterity = getDexterity() + dexterityNumber;
        var intelligenceUpdated = this.intelligence = getIntelligence() + intelligenceNumber;
        return new HeroAttribute(strengthUpdated, dexterityUpdated, intelligenceUpdated);
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    @Override
    public String toString() {
        return "HeroAttribute{" +
                "strength=" + strength +
                ", dexterity=" + dexterity +
                ", intelligence=" + intelligence +
                '}';
    }
}