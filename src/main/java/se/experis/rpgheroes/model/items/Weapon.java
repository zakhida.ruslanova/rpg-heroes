package se.experis.rpgheroes.model.items;

import se.experis.rpgheroes.model.items.enums.Slot;
import se.experis.rpgheroes.model.items.enums.WeaponType;

public class Weapon extends Item {
    private final WeaponType weaponType;
    private final int weaponDamage;

    public Weapon(String name, int requiredLevel, WeaponType weaponType, int weaponDamage) {
        super(name, requiredLevel, Slot.WEAPON);
        this.weaponType = weaponType;
        this.weaponDamage = weaponDamage;
    }
    public int getWeaponDamage() {
        return weaponDamage;
    }
    public WeaponType getWeaponType() {
        return weaponType;
    }
}