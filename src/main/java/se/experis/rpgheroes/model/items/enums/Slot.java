package se.experis.rpgheroes.model.items.enums;

public enum Slot {
    WEAPON, HEAD, BODY, LEGS;
}