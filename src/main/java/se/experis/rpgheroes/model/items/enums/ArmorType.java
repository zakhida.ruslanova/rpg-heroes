package se.experis.rpgheroes.model.items.enums;

public enum ArmorType {
    CLOTH, LEATHER, MAIL, PLATE
}