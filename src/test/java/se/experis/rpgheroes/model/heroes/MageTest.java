package se.experis.rpgheroes.model.heroes;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import se.experis.rpgheroes.model.exceptions.InvalidArmorException;
import se.experis.rpgheroes.model.exceptions.InvalidLevelException;
import se.experis.rpgheroes.model.exceptions.InvalidWeaponException;
import se.experis.rpgheroes.model.items.Armor;
import se.experis.rpgheroes.model.items.HeroAttribute;
import se.experis.rpgheroes.model.items.Weapon;
import se.experis.rpgheroes.model.items.enums.ArmorType;
import se.experis.rpgheroes.model.items.enums.Slot;
import se.experis.rpgheroes.model.items.enums.WeaponType;

class MageTest {

    Mage mage;

    @BeforeEach
    void setUp() {
        mage = new Mage("Ida");
    }

    @Test
    void verifyThatHeroAttributesIsEquals() {
        // Arrange
        HeroAttribute expectedHeroAttributeForMage = new HeroAttribute(1, 1, 8);
        int expectedLevelForMage = 1;
        // Act
        HeroAttribute actualHeroAttributeForMage = mage.getHeroAttributes("Mage");
        int actualLevelForMage = mage.getLevel();
        // Assert
        Assertions.assertEquals(expectedHeroAttributeForMage.getStrength(), actualHeroAttributeForMage.getStrength());
        Assertions.assertEquals(expectedHeroAttributeForMage.getDexterity(), actualHeroAttributeForMage.getDexterity());
        Assertions.assertEquals(expectedHeroAttributeForMage.getIntelligence(), actualHeroAttributeForMage.getIntelligence());
        Assertions.assertEquals(expectedLevelForMage, actualLevelForMage);
    }

    @Test
    void verifyHeroAttributeLevelUp() {
        // Arrange
        HeroAttribute heroAttributeForMage = new HeroAttribute(1, 1, 8);
        int strengthExpectedForMage = heroAttributeForMage.getStrength() + 1;
        int dexterityExpectedForMage = heroAttributeForMage.getDexterity() + 1;
        int intelligenceExpectedForMage = heroAttributeForMage.getIntelligence() + 5;
        int expectedLevelForMage = 2;
        // Act
        mage.levelUp();
        HeroAttribute actualAttributesForMage = mage.getLevelAttribute();
        int actualLevelForMage = mage.getLevel();
        // Assert
        Assertions.assertEquals(strengthExpectedForMage, actualAttributesForMage.getStrength());
        Assertions.assertEquals(dexterityExpectedForMage, actualAttributesForMage.getDexterity());
        Assertions.assertEquals(intelligenceExpectedForMage, actualAttributesForMage.getIntelligence());
        Assertions.assertEquals(expectedLevelForMage, actualLevelForMage);
    }

    @Test
    void verify_that_weapon_is_created_correctly() throws InvalidWeaponException, InvalidLevelException {
        // Arrange
        Weapon expectedWeapon = new Weapon("Ida", 1, WeaponType.WANDS, 10);
        // Act
        mage.equipWeapons(expectedWeapon);
        // Assert
        Weapon actualWeapon = (Weapon) mage.getEquipment().get(Slot.WEAPON);
        Assertions.assertEquals(expectedWeapon.getName(), actualWeapon.getName());
        Assertions.assertEquals(expectedWeapon.getRequiredLevel(), actualWeapon.getRequiredLevel());
        Assertions.assertEquals(expectedWeapon.getWeaponType(), actualWeapon.getWeaponType());
        Assertions.assertEquals(expectedWeapon.getWeaponDamage(), actualWeapon.getWeaponDamage());
    }

    @Test
    void verify_that_armor_is_created_correctly() throws InvalidArmorException, InvalidLevelException {
        // Arrange
        HeroAttribute armorAttributes = new HeroAttribute(1, 1, 1);
        Armor expectedArmor = new Armor("Ida", 1, Slot.BODY, ArmorType.CLOTH, armorAttributes);
        // Act
        mage.equipArmor(expectedArmor);
        // Assert

        Armor actualArmor = (Armor) mage.getEquipment().get(Slot.BODY);

        Assertions.assertEquals(expectedArmor.getName(), actualArmor.getName());
        Assertions.assertEquals(expectedArmor.getRequiredLevel(), actualArmor.getRequiredLevel());
        Assertions.assertEquals(expectedArmor.getSlot(), actualArmor.getSlot());
        Assertions.assertEquals(expectedArmor.getArmorType(), actualArmor.getArmorType());
        Assertions.assertEquals(expectedArmor.getArmorAttribute(), actualArmor.getArmorAttribute());
    }

    @Test
    void verify_that_equip_weapon_should_throw_exception_if_invalid_type() {
        // Arrange
        String expectedMessage = "Mage cannot equip weapon type BOWS";
        Weapon weapon = new Weapon("Ida", 2, WeaponType.BOWS, 10);
        mage.levelUp();
        // Act
        InvalidWeaponException exception = Assertions.assertThrows(InvalidWeaponException.class, () -> {
            mage.levelUp();
            mage.equipWeapons(weapon);
        });
        String actualMessage = exception.getMessage();
        // Assert
        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void verify_that_equip_armor_should_throw_exception_if_invalid_type() {
        // Arrange
        String expectedMessage = "Mage cannot equip armor type LEATHER";
        HeroAttribute armorAttributes = new HeroAttribute(1, 1, 1);
        Armor armor = new Armor("Ida", 2, Slot.HEAD, ArmorType.LEATHER, armorAttributes);
        mage.levelUp();
        // Act
        InvalidArmorException exception = Assertions.assertThrows(InvalidArmorException.class, () -> {
            mage.equipArmor(armor);
        });
        String actualMessage = exception.getMessage();
        // Assert
        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }
}