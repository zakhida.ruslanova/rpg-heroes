package se.experis.rpgheroes.model.heroes;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import se.experis.rpgheroes.model.exceptions.InvalidArmorException;
import se.experis.rpgheroes.model.exceptions.InvalidLevelException;
import se.experis.rpgheroes.model.exceptions.InvalidWeaponException;
import se.experis.rpgheroes.model.items.Armor;
import se.experis.rpgheroes.model.items.HeroAttribute;
import se.experis.rpgheroes.model.items.Weapon;
import se.experis.rpgheroes.model.items.enums.ArmorType;
import se.experis.rpgheroes.model.items.enums.Slot;
import se.experis.rpgheroes.model.items.enums.WeaponType;

class RangerTest {

    Ranger ranger;

    @BeforeEach
    void setUp() {
        ranger = new Ranger("Ida");
    }

    @Test
    void verifyThatHeroAttributesIsEquals() {
        // Arrange
        HeroAttribute expectedHeroAttributeForRanger = new HeroAttribute(1, 7, 1);
        int expectedLevelForRanger = 1;
        // Act
        HeroAttribute actualHeroAttributeForRanger = ranger.getHeroAttributes("Ranger");
        int actualLevelForRanger = ranger.getLevel();
        // Assert
        Assertions.assertEquals(expectedHeroAttributeForRanger.getStrength(), actualHeroAttributeForRanger.getStrength());
        Assertions.assertEquals(expectedHeroAttributeForRanger.getDexterity(), actualHeroAttributeForRanger.getDexterity());
        Assertions.assertEquals(expectedHeroAttributeForRanger.getIntelligence(), actualHeroAttributeForRanger.getIntelligence());
        Assertions.assertEquals(expectedLevelForRanger, actualLevelForRanger);
    }

    @Test
    void verifyHeroAttributeLevelUp() {
        // Arrange
        HeroAttribute heroAttributeForRanger = new HeroAttribute(1, 7, 1);
        int strengthExpectedForRanger = heroAttributeForRanger.getStrength() + 1;
        int dexterityExpectedForRanger = heroAttributeForRanger.getDexterity() + 5;
        int intelligenceExpectedForRanger = heroAttributeForRanger.getIntelligence() + 1;
        int expectedLevelForRanger = 2;
        // Act
        ranger.levelUp();
        HeroAttribute actualAttributesForRanger = ranger.getLevelAttribute();
        int actualLevelForRanger = ranger.getLevel();
        // Assert
        Assertions.assertEquals(strengthExpectedForRanger, actualAttributesForRanger.getStrength());
        Assertions.assertEquals(dexterityExpectedForRanger, actualAttributesForRanger.getDexterity());
        Assertions.assertEquals(intelligenceExpectedForRanger, actualAttributesForRanger.getIntelligence());
        Assertions.assertEquals(expectedLevelForRanger, actualLevelForRanger);
    }

    @Test
    void verify_that_weapon_is_created_correctly() throws InvalidWeaponException, InvalidLevelException {
        // Arrange
        Weapon expectedWeapon = new Weapon("Ida", 1, WeaponType.BOWS, 10);
        // Act
        ranger.equipWeapons(expectedWeapon);
        // Assert
        Weapon actualWeapon = (Weapon) ranger.getEquipment().get(Slot.WEAPON);
        Assertions.assertEquals(expectedWeapon.getName(), actualWeapon.getName());
        Assertions.assertEquals(expectedWeapon.getRequiredLevel(), actualWeapon.getRequiredLevel());
        Assertions.assertEquals(expectedWeapon.getWeaponType(), actualWeapon.getWeaponType());
        Assertions.assertEquals(expectedWeapon.getWeaponDamage(), actualWeapon.getWeaponDamage());
    }

    @Test
    void verify_that_armor_is_created_correctly() throws InvalidArmorException, InvalidLevelException {
        // Arrange
        HeroAttribute armorAttributes = new HeroAttribute(1, 1, 1);
        Armor expectedArmor = new Armor("Ida", 1, Slot.BODY, ArmorType.MAIL, armorAttributes);
        // Act
        ranger.equipArmor(expectedArmor);
        // Assert

        Armor actualArmor = (Armor) ranger.getEquipment().get(Slot.BODY);

        Assertions.assertEquals(expectedArmor.getName(), actualArmor.getName());
        Assertions.assertEquals(expectedArmor.getRequiredLevel(), actualArmor.getRequiredLevel());
        Assertions.assertEquals(expectedArmor.getSlot(), actualArmor.getSlot());
        Assertions.assertEquals(expectedArmor.getArmorType(), actualArmor.getArmorType());
        Assertions.assertEquals(expectedArmor.getArmorAttribute(), actualArmor.getArmorAttribute());
    }

    @Test
    void verify_that_equip_weapon_should_throw_exception_if_invalid_type() {
        // Arrange
        String expectedMessage = "Ranger cannot equip weapon type STAFFS";
        Weapon weapon = new Weapon("Ida", 2, WeaponType.STAFFS, 10);
        ranger.levelUp();
        // Act
        InvalidWeaponException exception = Assertions.assertThrows(InvalidWeaponException.class, () -> {
            ranger.levelUp();
            ranger.equipWeapons(weapon);
        });
        String actualMessage = exception.getMessage();
        // Assert
        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void verify_that_equip_armor_should_throw_exception_if_invalid_type() {
        // Arrange
        String expectedMessage = "Ranger cannot equip armor type CLOTH";
        HeroAttribute armorAttributes = new HeroAttribute(1, 1, 1);
        Armor armor = new Armor("Ida", 2, Slot.HEAD, ArmorType.CLOTH, armorAttributes);
        ranger.levelUp();
        // Act
        InvalidArmorException exception = Assertions.assertThrows(InvalidArmorException.class, () -> {
            ranger.equipArmor(armor);
        });
        String actualMessage = exception.getMessage();
        // Assert
        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }
}