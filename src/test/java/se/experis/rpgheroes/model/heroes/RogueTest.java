package se.experis.rpgheroes.model.heroes;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import se.experis.rpgheroes.model.exceptions.InvalidArmorException;
import se.experis.rpgheroes.model.exceptions.InvalidLevelException;
import se.experis.rpgheroes.model.exceptions.InvalidWeaponException;
import se.experis.rpgheroes.model.items.Armor;
import se.experis.rpgheroes.model.items.HeroAttribute;
import se.experis.rpgheroes.model.items.Weapon;
import se.experis.rpgheroes.model.items.enums.ArmorType;
import se.experis.rpgheroes.model.items.enums.Slot;
import se.experis.rpgheroes.model.items.enums.WeaponType;

class RogueTest {

    Rogue rogue;

    @BeforeEach
    void setUp() {
        rogue = new Rogue("Ida");
    }

    @Test
    void verifyThatHeroAttributesIsEquals() {
        // Arrange
        HeroAttribute expectedHeroAttributeForRogue = new HeroAttribute(2, 6, 1);
        int expectedLevelForRogue = 1;
        // Act
        HeroAttribute actualHeroAttributeForRogue = rogue.getHeroAttributes("Rogue");
        int actualLevelForRogue = rogue.getLevel();
        // Assert
        Assertions.assertEquals(expectedHeroAttributeForRogue.getStrength(), actualHeroAttributeForRogue.getStrength());
        Assertions.assertEquals(expectedHeroAttributeForRogue.getDexterity(), actualHeroAttributeForRogue.getDexterity());
        Assertions.assertEquals(expectedHeroAttributeForRogue.getIntelligence(), actualHeroAttributeForRogue.getIntelligence());
        Assertions.assertEquals(expectedLevelForRogue, actualLevelForRogue);
    }

    @Test
    void verifyHeroAttributeLevelUp() {
        // Arrange
        HeroAttribute heroAttributeForRogue = new HeroAttribute(2, 6, 1);
        int strengthExpectedForRogue = heroAttributeForRogue.getStrength() + 1;
        int dexterityExpectedForRogue = heroAttributeForRogue.getDexterity() + 4;
        int intelligenceExpectedForRogue = heroAttributeForRogue.getIntelligence() + 1;
        int expectedLevelForRogue = 2;
        // Act
        rogue.levelUp();
        int actualLevelForRogue = rogue.getLevel();
        HeroAttribute actualAttributesForRogue = rogue.getLevelAttribute();
        // Assert
        Assertions.assertEquals(strengthExpectedForRogue, actualAttributesForRogue.getStrength());
        Assertions.assertEquals(dexterityExpectedForRogue, actualAttributesForRogue.getDexterity());
        Assertions.assertEquals(intelligenceExpectedForRogue, actualAttributesForRogue.getIntelligence());
        Assertions.assertEquals(expectedLevelForRogue, actualLevelForRogue);
    }

    @Test
    void verify_that_weapon_is_created_correctly() throws InvalidWeaponException, InvalidLevelException {
        // Arrange
        Weapon expectedWeapon = new Weapon("Ida", 1, WeaponType.DAGGERS, 10);
        // Act
        rogue.equipWeapons(expectedWeapon);
        // Assert
        Weapon actualWeapon = (Weapon) rogue.getEquipment().get(Slot.WEAPON);
        Assertions.assertEquals(expectedWeapon.getName(), actualWeapon.getName());
        Assertions.assertEquals(expectedWeapon.getRequiredLevel(), actualWeapon.getRequiredLevel());
        Assertions.assertEquals(expectedWeapon.getWeaponType(), actualWeapon.getWeaponType());
        Assertions.assertEquals(expectedWeapon.getWeaponDamage(), actualWeapon.getWeaponDamage());
    }

    @Test
    void verify_that_armor_is_created_correctly() throws InvalidArmorException, InvalidLevelException {
        // Arrange
        HeroAttribute armorAttributes = new HeroAttribute(1, 1, 1);
        Armor expectedArmor = new Armor("Ida", 1, Slot.BODY, ArmorType.MAIL, armorAttributes);
        // Act
        rogue.equipArmor(expectedArmor);
        // Assert

        Armor actualArmor = (Armor) rogue.getEquipment().get(Slot.BODY);

        Assertions.assertEquals(expectedArmor.getName(), actualArmor.getName());
        Assertions.assertEquals(expectedArmor.getRequiredLevel(), actualArmor.getRequiredLevel());
        Assertions.assertEquals(expectedArmor.getSlot(), actualArmor.getSlot());
        Assertions.assertEquals(expectedArmor.getArmorType(), actualArmor.getArmorType());
        Assertions.assertEquals(expectedArmor.getArmorAttribute(), actualArmor.getArmorAttribute());
    }

    @Test
    void verify_that_equip_weapon_should_throw_exception_if_invalid_type() {
        // Arrange
        String expectedMessage = "Rogue cannot equip weapon type BOWS";
        Weapon weapon = new Weapon("Ida", 2, WeaponType.BOWS, 10);
        rogue.levelUp();
        // Act
        InvalidWeaponException exception = Assertions.assertThrows(InvalidWeaponException.class, () -> {
            rogue.levelUp();
            rogue.equipWeapons(weapon);
        });
        String actualMessage = exception.getMessage();
        // Assert
        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void verify_that_equip_armor_should_throw_exception_if_invalid_type() {
        // Arrange
        String expectedMessage = "Rogue cannot equip armor type CLOTH";
        HeroAttribute armorAttributes = new HeroAttribute(1, 1, 1);
        Armor armor = new Armor("Ida", 2, Slot.HEAD, ArmorType.CLOTH, armorAttributes);
        rogue.levelUp();
        // Act
        InvalidArmorException exception = Assertions.assertThrows(InvalidArmorException.class, () -> {
            rogue.equipArmor(armor);
        });
        String actualMessage = exception.getMessage();
        // Assert
        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }
}