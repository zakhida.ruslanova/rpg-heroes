package se.experis.rpgheroes.model.heroes;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import se.experis.rpgheroes.model.exceptions.InvalidArmorException;
import se.experis.rpgheroes.model.exceptions.InvalidLevelException;
import se.experis.rpgheroes.model.exceptions.InvalidWeaponException;
import se.experis.rpgheroes.model.items.Armor;
import se.experis.rpgheroes.model.items.HeroAttribute;
import se.experis.rpgheroes.model.items.Weapon;
import se.experis.rpgheroes.model.items.enums.ArmorType;
import se.experis.rpgheroes.model.items.enums.Slot;
import se.experis.rpgheroes.model.items.enums.WeaponType;

class WarriorTest {

    Warrior warrior;

    @BeforeEach
    void setUp() {
        warrior = new Warrior("Ida");
    }

    @Test
    void verifyThatHeroAttributesIsEquals() {
        // Arrange
        HeroAttribute expectedHeroAttributeForWarrior = new HeroAttribute(5, 2, 1);
        int expectedLevelForWarrior = 1;
        // Act
        HeroAttribute actualHeroAttributeForWarrior = warrior.getHeroAttributes("Warrior");
        int actualLevelForWarrior = warrior.getLevel();
        // Assert
        Assertions.assertEquals(expectedHeroAttributeForWarrior.getStrength(), actualHeroAttributeForWarrior.getStrength());
        Assertions.assertEquals(expectedHeroAttributeForWarrior.getDexterity(), actualHeroAttributeForWarrior.getDexterity());
        Assertions.assertEquals(expectedHeroAttributeForWarrior.getIntelligence(), actualHeroAttributeForWarrior.getIntelligence());
        Assertions.assertEquals(expectedLevelForWarrior, actualLevelForWarrior);
    }

    @Test
    void verifyHeroAttributeLevelUp() {
        // Arrange
        HeroAttribute heroAttributeForWarrior = new HeroAttribute(5, 2, 1);
        int strengthExpectedForWarrior = heroAttributeForWarrior.getStrength() + 3;
        int dexterityExpectedForWarrior = heroAttributeForWarrior.getDexterity() + 2;
        int intelligenceExpectedForWarrior = heroAttributeForWarrior.getIntelligence() + 1;
        int expectedLevelForWarrior = 2;
        // Act
        warrior.levelUp();
        int actualLevelForWarrior = warrior.getLevel();
        HeroAttribute actualAttributesForWarrior = warrior.getLevelAttribute();
        // Assert
        Assertions.assertEquals(strengthExpectedForWarrior, actualAttributesForWarrior.getStrength());
        Assertions.assertEquals(dexterityExpectedForWarrior, actualAttributesForWarrior.getDexterity());
        Assertions.assertEquals(intelligenceExpectedForWarrior, actualAttributesForWarrior.getIntelligence());
        Assertions.assertEquals(expectedLevelForWarrior, actualLevelForWarrior);
    }

    @Test
    void verify_that_weapon_is_created_correctly() throws InvalidWeaponException, InvalidLevelException {
        // Arrange
        Weapon expectedWeapon = new Weapon("Ida", 1, WeaponType.AXES, 10);
        // Act
        warrior.equipWeapons(expectedWeapon);
        // Assert
        Weapon actualWeapon = (Weapon) warrior.getEquipment().get(Slot.WEAPON);
        Assertions.assertEquals(expectedWeapon.getName(), actualWeapon.getName());
        Assertions.assertEquals(expectedWeapon.getRequiredLevel(), actualWeapon.getRequiredLevel());
        Assertions.assertEquals(expectedWeapon.getWeaponType(), actualWeapon.getWeaponType());
        Assertions.assertEquals(expectedWeapon.getWeaponDamage(), actualWeapon.getWeaponDamage());
    }

    @Test
    void verify_that_armor_is_created_correctly() throws InvalidArmorException, InvalidLevelException {
        // Arrange
        HeroAttribute armorAttributes = new HeroAttribute(1, 1, 1);
        Armor expectedArmor = new Armor("Ida", 1, Slot.BODY, ArmorType.MAIL, armorAttributes);
        // Act
        warrior.equipArmor(expectedArmor);
        // Assert

        Armor actualArmor = (Armor) warrior.getEquipment().get(Slot.BODY);

        Assertions.assertEquals(expectedArmor.getName(), actualArmor.getName());
        Assertions.assertEquals(expectedArmor.getRequiredLevel(), actualArmor.getRequiredLevel());
        Assertions.assertEquals(expectedArmor.getSlot(), actualArmor.getSlot());
        Assertions.assertEquals(expectedArmor.getArmorType(), actualArmor.getArmorType());
        Assertions.assertEquals(expectedArmor.getArmorAttribute(), actualArmor.getArmorAttribute());
    }

    @Test
    void verify_that_equip_weapon_should_throw_exception_if_invalid_type() {
        // Arrange
        String expectedMessage = "Warrior cannot equip weapon type BOWS";
        Weapon weapon = new Weapon("Ida", 2, WeaponType.BOWS, 10);
        warrior.levelUp();
        // Act
        InvalidWeaponException exception = Assertions.assertThrows(InvalidWeaponException.class, () -> {
            warrior.levelUp();
            warrior.equipWeapons(weapon);
        });
        String actualMessage = exception.getMessage();
        // Assert
        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void verify_that_equip_armor_should_throw_exception_if_invalid_type() {
        // Arrange
        String expectedMessage = "Warrior cannot equip armor type LEATHER";
        HeroAttribute armorAttributes = new HeroAttribute(1, 1, 1);
        Armor armor = new Armor("Ida", 2, Slot.HEAD, ArmorType.LEATHER, armorAttributes);
        warrior.levelUp();
        // Act
        InvalidArmorException exception = Assertions.assertThrows(InvalidArmorException.class, () -> {
            warrior.equipArmor(armor);
        });
        String actualMessage = exception.getMessage();
        // Assert
        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }
}