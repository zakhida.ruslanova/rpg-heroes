package se.experis.rpgheroes.model.heroes;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import se.experis.rpgheroes.model.exceptions.InvalidArmorException;
import se.experis.rpgheroes.model.exceptions.InvalidLevelException;
import se.experis.rpgheroes.model.exceptions.InvalidWeaponException;
import se.experis.rpgheroes.model.items.Armor;
import se.experis.rpgheroes.model.items.HeroAttribute;
import se.experis.rpgheroes.model.items.Item;
import se.experis.rpgheroes.model.items.Weapon;
import se.experis.rpgheroes.model.items.enums.ArmorType;
import se.experis.rpgheroes.model.items.enums.Slot;
import se.experis.rpgheroes.model.items.enums.WeaponType;

import java.util.HashMap;

class HeroTest {
    Mage mage;
    Ranger ranger;
    Rogue rogue;
    Warrior warrior;

    @BeforeEach
    void setUp() {
        mage = new Mage("Ida");
        ranger = new Ranger("Ida");
        rogue = new Rogue("Ida");
        warrior = new Warrior("Ida");
    }

    @Test
    void verifyThatHeroAttributesShouldThrowIllegalArgumentException() {
        // Arrange
        String expectedMessage = "The name Hero is not match heroes name";
        // Act
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            mage.getHeroAttributes("Hero");
        });
        String actualMessage = exception.getMessage();
        // Assert
        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void verifyThatHeroDefaultEquipmentEquals() {
        // Arrange
        HashMap<Slot, Item> expected = new HashMap<>();
        expected.put(Slot.BODY, null);
        expected.put(Slot.HEAD, null);
        expected.put(Slot.LEGS, null);
        expected.put(Slot.WEAPON, null);
        // Act
        HashMap<Slot, Item> defaultEquipment = mage.getDefaultEquipment();
        // Assert
        Assertions.assertEquals(expected, defaultEquipment);
    }

    @Test
    void verify_that_equip_weapon_should_throw_exception_if_invalid_level() {
        // Arrange
        String expectedMessage = "2 is not enougth. The required level is: 5";
        Weapon weapon = new Weapon("Ida", 5, WeaponType.WANDS, 10);
        // Act
        InvalidLevelException exception = Assertions.assertThrows(InvalidLevelException.class, () -> {
            mage.levelUp();
            mage.equipWeapons(weapon);
        });
        String actualMessage = exception.getMessage();
        // Assert
        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void verify_that_equip_armor_should_throw_exception_if_invalid_level() {
        String expectedMessage = "2 is not enougth. The required level is: 5";
        HeroAttribute armorAttributes = new HeroAttribute(1, 1, 1);
        Armor armor = new Armor("Ida", 5, Slot.HEAD, ArmorType.CLOTH, armorAttributes);
        mage.levelUp();
        // Act
        InvalidLevelException exception = Assertions.assertThrows(InvalidLevelException.class, () -> {
            mage.equipArmor(armor);
        });
        String actualMessage = exception.getMessage();
        // Assert
        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void verify_total_attributes_calculated_correctly_with_no_equipment() {
        // Arrange
        String expectedMessage = "Total Attribute: 10";
        // Act
        String actualMeassage = mage.totalAttribute();
        // Assert
        Assertions.assertEquals(expectedMessage, actualMeassage);
    }

    @Test
    void verify_total_attributes_calculated_correctly_with_one_piece_of_armor() throws InvalidArmorException, InvalidLevelException {
        // Arrange
        String expectedMessage = "Total Attribute: 13";
        // Act
        HeroAttribute armorAttributes = new HeroAttribute(1, 1, 1);
        Armor armor = new Armor("Ida", 1, Slot.HEAD, ArmorType.CLOTH, armorAttributes);
        mage.equipArmor(armor);
        String actualMeassage = mage.totalAttribute();
        // Assert
        Assertions.assertEquals(expectedMessage, actualMeassage);
    }

    @Test
    void verify_total_attributes_calculated_correctly_with_two_piece_of_armor() throws InvalidArmorException, InvalidLevelException {
        // Arrange
        String expectedMessage = "Total Attribute: 15";
        // Act
        HeroAttribute armorAttributes1 = new HeroAttribute(1, 1, 1);
        Armor armor1 = new Armor("Ida", 1, Slot.HEAD, ArmorType.LEATHER, armorAttributes1);
        HeroAttribute armorAttributes2 = new HeroAttribute(1, 1, 1);
        Armor armor2 = new Armor("Ida", 1, Slot.LEGS, ArmorType.MAIL, armorAttributes2);
        ranger.equipArmor(armor1);
        ranger.equipArmor(armor2);
        String actualMeassage = ranger.totalAttribute();
        // Assert
        Assertions.assertEquals(expectedMessage, actualMeassage);
    }

    @Test
    void verify_total_attributes_calculated_correctly_with_replaced_piece_of_armor() throws InvalidArmorException, InvalidLevelException {
        // Arrange
        String expectedMessage = "Total Attribute: 12";
        // Act
        HeroAttribute armorAttributes1 = new HeroAttribute(1, 1, 1);
        Armor armor1 = new Armor("Ida", 1, Slot.HEAD, ArmorType.LEATHER, armorAttributes1);
        HeroAttribute armorAttributes2 = new HeroAttribute(1, 1, 1);
        Armor armor2 = new Armor("Ida", 1, Slot.HEAD, ArmorType.MAIL, armorAttributes2);
        ranger.equipArmor(armor1);
        ranger.equipArmor(armor2);
        String actualMeassage = ranger.totalAttribute();
        // Assert
        Assertions.assertEquals(expectedMessage, actualMeassage);
    }

    @Test
    void verify_damage_calculated_correctly_with_no_equipment() {
        // Arrange
        double expectedDamage = 1.08;
        // Act
        double actualDamage = mage.damage();
        // Assert
        Assertions.assertEquals(expectedDamage, actualDamage);
    }

    @Test
    void verify_damage_calculated_correctly_with_one_piece_of_weapon() throws InvalidWeaponException, InvalidLevelException {
        // Arrange
        double expectedDamage = 10.8;
        // Act
        Weapon weapon = new Weapon("Ida", 1, WeaponType.STAFFS, 10);
        mage.equipWeapons(weapon);
        double actualDamage = mage.damage();
        // Assert
        Assertions.assertEquals(expectedDamage, actualDamage);
    }

    @Test
    void verify_damage_calculated_correctly_with_replaced_weapon_eqipped() throws InvalidWeaponException, InvalidLevelException {
        // Arrange
        double expectedDamage = 10.8;
        // Act
        Weapon weapon1 = new Weapon("Ida", 1, WeaponType.STAFFS, 10);
        Weapon weapon2 = new Weapon("Ida", 1, WeaponType.STAFFS, 10);
        mage.equipWeapons(weapon1);
        mage.equipWeapons(weapon2);
        double actualDamage = mage.damage();
        // Assert
        Assertions.assertEquals(expectedDamage, actualDamage);
    }

    @Test
    void verify_damage_calculated_correctly_with_weapon_and_armor_eqipped() throws InvalidWeaponException, InvalidLevelException, InvalidArmorException {
        // Arrange
        double expectedDamage = 10.8;
        // Act
        Weapon weapon = new Weapon("Ida", 1, WeaponType.STAFFS, 10);
        HeroAttribute armorAttributes = new HeroAttribute(1, 1, 1);
        Armor armor = new Armor("Ida", 1, Slot.HEAD, ArmorType.CLOTH, armorAttributes);
        mage.equipWeapons(weapon);
        mage.equipArmor(armor);
        double actualDamage = mage.damage();
        // Assert
        Assertions.assertEquals(expectedDamage, actualDamage);
    }

    @Test
    void verify_heroes_display_their_state_correctly() {
        // Arrange
        StringBuilder expectedDisplay = new StringBuilder();
        expectedDisplay.append("Name: Ida");
        expectedDisplay.append("\n" + "Class: Mage");
        expectedDisplay.append("\n" + "Level: 1");
        expectedDisplay.append("\n" + "Total strength: 1");
        expectedDisplay.append("\n" + "Total dexterity: 1");
        expectedDisplay.append("\n" + "Total intelligence: 8");
        expectedDisplay.append("\n" + "Damage: 1.08");
        // Act
        String actualDisplay = mage.display();
        // Assert
        Assertions.assertEquals(expectedDisplay.toString(), actualDisplay);
    }
}